import axios from '@/libs/api.request'

export const getRouterReq = () => {
  return axios.request({
    url: '/config/router/',
    method: 'get'
  })
}
