import env from './env'

const DEV_URL = 'http://192.168.2.100:8060'
const PRO_URL = 'https://produce.com'

export default env === 'development' ? DEV_URL : PRO_URL
